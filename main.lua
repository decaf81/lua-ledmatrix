-- Leds horizontaal: 59
-- Leds vertikkaal: 5
fullchr = {1,2,3,60,61,62,119,120,121,178,179,180,237,238,239} 
letter_a = {2,60,62,119,120,121,178,180,237,239}
letter_c = {1,2,3,60,119,178,237,238,239}
letter_d = {1,2,60,62,119,121,178,180,237,238}
letter_e = {1,2,3,60,119,120,178,237,238,239}
letter_f = {1,2,3,60,119,120,121,178,237}
letter_g = {1,2,3,60,62,119,120,121,180,237,238,239}
letter_h = {1,3,60,62,119,120,121,178,180,237,239}
letter_i = {2,61,120,179,238}
letter_j = {3,62,121,180,239,238,178}
letter_k = {1,60,119,178,237,120,3,239}
letter_l = {1,60,119,178,237,238,239}
letter_m = {1,61,62,3,60,119,178,237,121,180,239}
letter_n = {1,2,3,60,62,119,121,178,180,237,239}
letter_o = {1,2,3,60,62,119,121,178,180,237,238,239}
letter_p = {1,2,3,60,62,119,120,121,178,237}
letter_q = {1,2,3,60,62,119,120,121,180,239}
letter_r = {1,2,3,60,62,119,120,121,178,179,237,239}
letter_s = {1,2,3,60,119,120,121,180,237,238,239}
letter_t = {2,61,120,179,238,1,3}
letter_u = {1,3,60,62,119,121,178,180,237,238,239}
letter_v = {1,3,60,62,119,121,178,180,238}
letter_x = {1,3,61,120,179,237,239}
letter_y = {1,3,60,62,120,179,238}
letter_z = {1,2,3,62,120,178,237,238,239}
rightarrow = {1,61,121,179,237,2,62,122,180,238}
charbuffer = {}
charbufferIndex = 0
pos = 0
command={}
anibuffer={}
anibufferIndex=0


-- 1 .. 3
-- 60 .. 62
-- 119 .. 121
-- 178 .. 180
-- 237 .. 239

PIXELS     = 8
TIME_ALARM = 25      -- 0.025 second, 40 Hz
TIME_SLOW  = 500000  -- 0.500 second,  2 Hz
US_TO_MS = 1000
LEDS = 330
WIDTH = 59
ROWS = 5
stripe = 0
ws2812.init()
buffer = ws2812.newBuffer(LEDS, 3)
buffer2 = ws2812.newBuffer(LEDS, 3)
buffer:fill(0,0,0)
ws2812.write(buffer)
char_with=4


functions = {

}
function resetCharBuffer()
 charbuffer = {}
 charbufferIndex = 0
 end
 
function wait(time,sub,arg0,arg1,arg2,arg3)
  tempsub = 0
  tomeout=tonumber(time)
  print("Wait function called. Sub: ",sub, "Arg ",arg0,arg1,arg2,arg3)
  tmr.alarm(3, time, tmr.ALARM_SINGLE, function()
   tempsub = tempsub + 1
   print("Timer running")
   if not sub(arg0,arg1,arg2,arg3) then
     print("Fixed some issues")
   end 
   end)
end

function printToConsole(args)
 print(args)
end

function bufferfadeIn(r,g,b)
  print("BufferFadein called R:",r,"G:",g,"B:",b) 
  for _,s in pairs(charbuffer) do 
    s = s
    buffer:set(s, {255, 255, 255})
    ws2812.write(buffer)
    tmr.delay(10000)
    buffer:set(s, {r, g, b})
    ws2812.write(buffer)
  end
end

function bufferfill(what,position)
  for _,s in pairs(what)  do 
  anibufferIndex = anibufferIndex + 1
  anibuffer[anibufferIndex] = s
  end
end

function bufferstring(str,position)
pos = position
_G.prefpos = pos
prevstr = str
used = 1
_G.prevlength = #str

print("String is", str,"Position is",position)
 for i = 1, #str do
  local karakter = (str:sub(i,i))
  for _,s in pairs(_G["letter_"..karakter])  do 
  s = s + pos
  charbufferIndex = charbufferIndex + 1
  charbuffer[charbufferIndex] = s
  y = s + pos
--    buffer:set(y, {0, 5, 5})
--    print(s)
  end
  pos = pos + 4
end

end
    -- do something with c

function bufferprint(offset,r,g,b)
  for _,s in pairs(charbuffer) do 
    s = s + offset
    buffer:set(s, {r, g, b})
  end
ws2812.write(buffer)
end



function charbufferclear()
    charbuffer = {}
    charbufferIndex = 0
end

function bufferfadeOut()
  for _,s in pairs(charbuffer) do 
    s = s 
    buffer:set(s, {255, 255, 255})
    ws2812.write(buffer)
    tmr.delay(10000)
    buffer:set(s, {0, 0, 0})
    ws2812.write(buffer)
  end
end

-- function wait(seconds)
--    start = tmr.now()
--    microseconds = seconds * 1000000
--    tmr.alarm(6, 10, tmr.ALARM_AUTO, function()
--    einde = tmr.now()
--    elapsed = einde - start
--    if elapsed > microseconds then
--       tmr.stop(6)
--    end
--    
--    end)
--  end
  
function blinksem()
x = 21
-- fill pixels -- 
 pos = 1 + x
 for _,s in pairs(letter_s)  do 
  y = s + pos
  buffer:set(y, {0, 5, 5})
  print(s)
 end
 pos = 5 + x
 for _,s in pairs(letter_e) do 
  y = s + pos
  buffer:set(y, {0, 5, 5})
  print(s)
 end
 for _,s in pairs(letter_m) do 
  pos = 9 + x
  y = s + pos
  buffer:set(y, {0, 5, 5})
  print(s)
 end
 
 ws2812.write(buffer)
 tmr.delay(10000)

 

end


function genrandom()

end
function blink(led,timeout,r,g,b)
  buffer2:set(led, {r,g, b})
  ws2812.write(buffer2)
  buffer2:set(led, {0, 0, 0})
  ws2812.write(buffer2)
end

function stars()
 print ("stars function called")
 tmr.alarm(0, 200, tmr.ALARM_AUTO, function()
 if enableStars == 1 then
   stran1 = node.random(330)
   stran2 = node.random(330)
   stran3 = node.random(330)
    ok1 = 0
    ok2 = 0
--    if charbuffer[1] == nil then 
   blink(stran1,200,10,20)
   blink(stran2,200,10,20)   
   blink(stran3,200,10,20)          
--    else

--      for _,s in pairs(charbuffer2) do
--            if s ~= stran1 then
--                tmr.alarm(3, 10, tmr.ALARM_SINGLE, function()
--                blink(stran1,200,10,20)
--                end)
--            else print("Mis")
--           end
            
--    tmr.wdclr()
--            if s ~= stran2 then
--                tmr.alarm(3, 10, tmr.ALARM_SINGLE, function()
--                blink(stran2,200,10,20)
--                end)
--            else print("Mis")
--            end
   
else
tmr.stop(0)
end
end)

end

function swipearrow(positions,r,g,b)
  bufferfill(rightarrow)
    pos = 0
    for pos=0,positions do
    tmr.wdclr()
     if pos~= 0 then
     buffer:fill(0,2,0)
     ws2812.write(buffer)
     end
     for _,s in pairs(anibuffer) do
     tmr.alarm(3, 100, tmr.ALARM_SINGLE, function() 
     s = s + pos
     buffer:set(s, {r, g, b})
     end)
     
     end
     ws2812.write(buffer)
 
  end
  anibuffer={}
  anibufferIndex=0
end

function colorswipe(r,g,b)
  s = 1
   for dot=1,WIDTH do
    s = dot
    for p=1,ROWS do 
    tmr.wdclr()
     buffer:set(s,r,g,b)
     s = s + WIDTH
     ws2812.write(buffer)
     end
   
   end
  ws2812.write(buffer)
  end
   
-- buffer:fill(0,1,0)
-- ws2812.write(buffer)
-- tmr.delay(9200)
-- swipearrow(51,255,255,255)

-- wait(5000,print,("Well yeak"))
-- wait(5000,bufferfadeIn,1,0,255)
-- wait(5000,bufferstring,1,0,255)
-- print("done waiting")

-- bufferfadeOut()
-- charbufferclear()

-- bufferfadeIn(1,0,255)
-- tmr.delay(900000)
-- bufferfadeOut()
-- charbufferclear()



